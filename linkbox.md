# link-box

## cursos

-   [NodeSchool](https://nodeschool.io/pt-br/)
-   <https://github.com/selfteaching/free-programming-books>

## ferramentas

-   <https://github.com/RSS-Bridge/rss-bridge>
-   <https://github.com/awdeorio/mailmerge/>
-   [GitHub - JustComments/newsletter-cli: Write newsletters in Markdown and send to many recipients from your machine using AWS SES](https://github.com/JustComments/newsletter-cli)
-   [GitHub - raphiz/nele: Send fancy markdown newsletters](https://github.com/raphiz/nele)
-   [dillinger](https://dillinger.io/)
-   [captcheck](https://source.netsyms.com/Netsyms/Captcheck) (google-free captcha)

## android

-   [T-UI Launcher](https://tui.tarunshankerpandey.com/)

## linux

-   [Show HN: Distro.tools - Scripts for lazy Linux users](https://distro.tools/)
-   [Installing | Project Hamster](https://projecthamster.wordpress.com/building-and-running/)
-   <https://gtimelog.org/>

## github, awesome etc

-   <https://github.com/alebcay/awesome-shell>
-   <https://github.com/aviaryan/awesome-no-login-web-apps#readme>
-   <https://github.com/rhythmus/markdown-resources/blob/master/markdown-tools.md>
-   <https://github.com/tevko/Resources>
-   <https://github.com/starandtina/The-Terrible-Front-End-List/blob/master/links.md>
-   <https://github.com/moklick/frontend-stuff>
-   [GitHub - jonathandion/awesome-emails: ✉️ An awesome list of resources to build better emails.](https://github.com/jonathandion/awesome-emails)
-   [GitHub - caspian-seagull/awesome-emails: List of tools, articles and videos about email design and development](https://github.com/caspian-seagull/awesome-emails)
-   <https://github.com/neiesc/awesome-minimalist>
-   [webpro/awesome-dotfiles: A curated list of dotfiles resources.](https://github.com/webpro/awesome-dotfiles)
-   <https://github.com/BubuAnabelas/awesome-markdown>
-   <https://github.com/dvorka/awesome-markdown-repositories>
-   2019-04-28 <https://github.com/ripienaar/free-for-dev>
-   2019-04-28 <https://github.com/tvvocold/FOSS-for-Dev>

## docs

-   [docsify](https://docsify.js.org/) \| [awesome docsify](https://github.com/docsifyjs/awesome-docsify)
-   [nunjucks](https://mozilla.github.io/nunjucks/templating.html)

## php & twig

-   [Twig for Template Designers - Documentation - Twig - The flexible, fast, and secure PHP template engine](https://twig.symfony.com/doc/2.x/templates.html)

## js cdn

-   [jsdelivr](https://www.jsdelivr.com/)
-   [cdnjs](https://cdnjs.com/)

## sysadmin

-   [Found a Free Unlimited Webhost](https://www.reddit.com/r/freewebhosting/comments/asgnr1/found_a_free_unlimited_webhost/)
-   [Host Koala - Super Cheap cPanel Hosting](https://hostkoala.com/)

## markdown

-   [manuscripts](http://manuscripts.github.io/) | <https://github.com/manuscripts>
-   <https://github.com/commenthol/md-fileserver>
-   <https://markdown-here.com/> \| <https://github.com/adam-p/markdown-here>
-   <https://github.com/bookdesigns>
-   <https://github.com/hybook> \| <https://github.com/book-templates>

## blog/site/newsletter

-   [expose](https://github.com/Jack000/expose)
-   <https://www.netlify.com/docs/>
-   <http://staticsiteboilerplate.com/>
-   <https://formspree.io/>
-   <https://mozilla.github.io/nunjucks/templating.html>
-   <https://cssfx.dev/>
-   <https://mjml.io/documentation/>
-   [Looking for Image gallery script without database](https://www.reddit.com/r/selfhosted/comments/bfp40b/looking_for_image_gallery_script_without_database/)
-   [surge.sh](https://surge.sh/)

## later

-   [Manage your daily schedule with Git](https://opensource.com/article/19/4/calendar-git)
-   [Removing sensitive data from a repository - GitHub Help](https://help.github.com/en/articles/removing-sensitive-data-from-a-repository)
-   [github/gitignore: A collection of useful .gitignore templates](https://github.com/github/gitignore)
-   [Elegant READMEs](https://www.yegor256.com/2019/04/23/elegant-readme.html)
-   [referência p/ site de livro aberto](http://craftinginterpreters.com/contents.html)
-   [tema para página de fotos;](https://github.com/klugjo/hexo-theme-magnetic)
-   <https://github.com/una/CSSgram> +link
-   [the federation - a statistics hub](https://the-federation.info/pixelfed)
-   [Openbox - ArchWiki](https://wiki.archlinux.org/index.php/Openbox)
-   [dashboard](https://github.com/darekkay/dashboard)
-   [The Disconnect](https://thedisconnect.co/three)
-   [Alternative to reCAPTCHA](https://www.reddit.com/r/degoogle/comments/b8qtv3/alternative_to_recaptcha/)
-   [SkimFeed – A Tech News Dashboard](https://skimfeed.com/)
-   [EarDoor.com - learn german by listening and reading](https://eardoor.com/)
-   [What are some Privacy sites that list privacy tools?](https://www.reddit.com/r/privacytoolsIO/comments/bft6wy/what_are_some_privacy_sites_that_list_privacy/)
- [txti](http://txti.es/)
- [rwtxt](https://rwtxt.com/rwtxt/about)
- [The Big List of Mobile Games](https://docs.google.com/spreadsheets/d/1bf0OxtVxrboZqyEh01AxJYUUqHm8tEfh-Lx-SugcrzY/htmlview)
